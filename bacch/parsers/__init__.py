
def find_files(path, exts):
    paths = []
    for ext in exts:
        for src in path.rglob(ext):
            paths.append(src)
    return paths


