
import re
from marko.renderer import Renderer

newline = re.compile("\s*\n+")

class LaTeXRenderer(Renderer):

    def _render_children(self, element):
        if isinstance(element, str):
            print("STRING FOUND:\n")
            print(element)
            print("\n\n")
            return ""
        else:
            rendered = [self.render(child) for child in element.children]  # type: ignore
            return "".join(rendered)

    def render_blank_line(self, element):
        return ""

    def render_line_break(self, element):
        return "\n"

    def render_raw_text(self, element):
        return element.children 

    def render_plain_text(self, element):
        if isinstance(element.children, str):
            return element.children

    ###################### BLOCK ELEMENTS #############################
    def render_paragraph(self, element):
        children = self.render_children(element)
        return f"\n\n{children}\n\n"

    def render_list(self, element):
        children = self.render_children(element)
        if element.ordered:
            list_type = "enumerate"
        else:
            list_type = "itemize"
        return "\n\n\\begin{%s}\n\n%s\n\n\\end{%s}\n\n" % (list_type, children, list_type)

    def render_list_item(self, element):
        children = self.render_children(element)
        return "\item %s" % children

    def render_heading(self, element):
        if True:
            match element.level:
                case 1:
                    sect = "part"
                case 2:
                    sect = "chapter"
                case 3:
                    sect = "section"
                case 4:
                    sect = "subsection"
                case 5:
                    sect = "subsubsection"
                case 6:
                    sect = "paragraph"
                case _:
                    sect = "subparagraph"
        else:
            match element.level:
                case 1:
                    sect = "chapter"
                case 2:
                    sect = "section"
                case 3:
                    sect = "subsection"
                case 4:
                    sect = "subsubsection"
                case 5:
                    sect = "paragraph"
                case _:
                    sect = "subparagraph"
        return "\\%s*{%s}\n\n" % (sect, self.render_children(element))

    def render_outline(self, element):
        return "\\begin{center}\n%s\n\\end{center}" % self.render_children(element)

    def render_quote(self, element):
        return "\\begin{displayquote}\\small\\linespread{1.0}\\ttfamily\n%s\n\\end{displayquote}" % self.render_children(element)

    ############################# INLINE ELEMENTS ######################
    def render_literal(self, element):
        return "\\texttt{%s}" % self.render_children(element)

    def render_code_span(self, element):
        return "\\texttt{%s}" % self.render_children(element)

    def render_inline_html(self, element):
        return "\\texttt{INLINE_HTML(%s)}" % self.render_children(element)

    def render_emphasis(self, element):
        return "\\emph{%s}" % self.render_children(element)

    def render_strong_emphasis(self, element):
        return "\\textsc{%s}" % self.render_children(element)




