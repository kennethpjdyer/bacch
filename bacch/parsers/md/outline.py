


import re
from marko import block

class Outline(block.BlockElement):
    pattern = re.compile(r";;;\s*\n[^\n\S]*\n;;;\s*$", re.M)
    parse_children = True

    def __init__(self, match):
        self.target = match.group(1)

class OutlineRendererMixin(object):
    def render_outline(self, element):
        return ""

class Outline:
    elements = [Outline]
    renderer_mixins = [OutlineRendererMixin]
