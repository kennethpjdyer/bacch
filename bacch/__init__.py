# Module Imports
import argparse
import bacch.build
import bacch.parsers.yml
import bacch.parsers.md
import logging
import pathlib
import sys
import yaml

def report_version(args):
    sys.exit(0)

def main():

    ######################## ARGUMNT PARSER ###################
    parser = argparse.ArgumentParser()
    parser.set_defaults(func=report_version)
    cmds = parser.add_subparsers(title="commands")

    # Configuration File
    parser.add_argument(
        "-c", "--config", default="./config.yml",
        help="Specifies the configuration file")

    # Verbose Messaging
    parser.add_argument(
        "-v", "--verbose", action="store_true",
        help="Enables verbose messaging"
    )

    cmd_build = cmds.add_parser(
        "build",
        help="Runs build operation"
    )

    cmd_build.set_defaults(func=bacch.build.run)
    cmd_build.add_argument(
        "--latex-only", action="store_true",
        help="Renders LaTeX output only, performs no additional process")
    cmd_build.add_argument(
        "target", nargs="*", default=['book'],
        help="Specifies build operation"
    )

    # Parse Arguments
    args = parser.parse_args()

    ############# CONFIGURE LOGGING ##############
    log_level = logging.WARN
    if args.verbose:
        log_level = logging.DEBUG

    logging.basicConfig(level=log_level)

    ############ CONFIGURE DATA ###################
    if args.func in [report_version]:
        args.func(args)

    # Load Configuration
    args.conf_path = pathlib.Path(args.config).resolve()
    args.config = bacch.parsers.yml.read_yaml(args.conf_path)

    # Set Paths
    args.source_path = args.conf_path.parent.joinpath(args.config.get("source", "src"))
    args.output = args.conf_path.parent.joinpath(args.config.get("output", "tmp"))

    # Read YaML Data
    data = {}
    bacch.parsers.yml.load_data(args.source_path, data)

    args.func(args)
    # Close
    sys.exit(0)




