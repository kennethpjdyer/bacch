# Module Imports
import pathlib
import re

# Function Imports
from distutils.core import setup


# Build Package List
packages = [
    "bacch",
    "bacch.build",
    "bacch.parsers",
    "bacch.parsers.md",
    "bacch.parsers.yml",
]
package_dirs = {}
dot = re.compile("\.")

for pkg in packages:
    src = re.sub(dot, "/", pkg)
    package_dirs[pkg] = src

# Configure Scripts
bin_path = pathlib.Path("scripts")
bins = []
for script in bin_path.glob("*"):
    if script.is_file():
        bins.append(script)


# Setup Package
setup(
    name="bacch",
    version="2022.4",
    packages=packages,
    scripts=bins,
)
